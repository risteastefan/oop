
import GraphClass
graph = GraphClass.Graph()

inputFile = open("input", "r")

graph.readGraph(inputFile)
graph.printGraph()
graph.printConnectedComponents()
print(graph.testIfIsCyclic())
print(graph.findShortestPath('D','C'))
print(graph.findLongestPath('D','C'))