def read(inputFile):
    param = inputFile.read(1) # Function used for reading a character and new line or space after
    inputFile.read(1)
    return param
class Graph(object):
    def __init__(self):
        self.m_graph = {}
        self.m_nrNodes = 0    #Initialize class variables
        self.m_isCyclic = False
        self.m_connectedComponents = {}
        self.m_nrComponents = 0

    def readGraph(self, inputFile):
        self.m_nrNodes = int(read(inputFile)) # Reads the number of nodes
        for i in range (0, self.m_nrNodes):
            node = read(inputFile)# Reads the node
            self.m_graph.setdefault(node, [])#initializse the node in dictionary
            nrNeighbours = int(read(inputFile))
            for j in range (nrNeighbours):#Reads the neighbours
                neighbour = read(inputFile)
                self.m_graph[node].append(neighbour)

    def printGraph(self):
        for node in self.m_graph:
            print(node, end = ' -> ') #Prints the graph
            for neighbour in self.m_graph[node]:
                print(neighbour, end = ' ')
            print()
    def printConnectedComponents(self):
        self.FullDFS()
        for node in self.m_connectedComponents:
            print (node, end = ' ')#Prints the connected components
            for neighbour in self.m_connectedComponents[node]:
                print(neighbour, end = ' ')
            print()
        self.m_nrComponents = 0
        self.m_connectedComponents = {}

    def testIfIsCyclic(self):
        self.FullDFS()
        return self.m_isCyclic
        self.m_nrComponents = 0
        self.m_connectedComponents = {}

    #This function goes through the elements of the connected components
        # and finds if the graph has cycles
    def ComponentDFS(self, node, visitList, parent, nrComponent):
        visitList.append(node)
        self.m_connectedComponents[nrComponent].append(node)
        for neighbour in self.m_graph[node]:
            if neighbour != parent: # if one of the neighbours is not the previously accesed node
                if neighbour not in visitList: #If it is not visited, then we keep searching
                    self.ComponentDFS(neighbour, visitList, node, nrComponent)
                else:
                    self.m_isCyclic = True # if it has been visiteed ,it means the graph has cycles

    def FullDFS(self):
        visitList = []
        for nodes in self.m_graph:
            if nodes not in visitList:#If this node hasn't been visited
                self.m_connectedComponents.setdefault(self.m_nrComponents, [])#Initialize the connected component
                self.ComponentDFS(nodes, visitList, nodes, self.m_nrComponents)#Go through the connected component
                self.m_nrComponents = self.m_nrComponents + 1#Increase the number of connected component

    def findShortestPath(self, start, end, path=[]):
        path = path + [start]
        if start == end: # if start point is the same as end point, then we have reached the destination
            return path
        shortest = None
        if start not in self.m_graph: #if the starting point is not in the graph , return none
            return None
        for nodes in self.m_graph[start]:
            if nodes not in path: #finds a new node in the path
                newPath = self.findShortestPath(nodes, end, path)#adds it to the path
                if newPath:
                    if not shortest or len(newPath) < len(shortest): # if the new path is longer than the new path becomes
                                                                        # the old one
                        shortest = newPath
        return shortest
    def findLongestPath(self, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        longest = None
        if start not in self.m_graph:
            return None
        for nodes in self.m_graph[start]:
            if nodes not in path:
                newPath = self.findLongestPath(nodes, end, path)
                if newPath:
                    if not longest or len(newPath) > len(longest):
                        longest = newPath
        return longest
