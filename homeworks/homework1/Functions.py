
inputFile = open("input","r")
graph = {}
nrNodes = 0
visitList = []
nrVisited = 0
full = False
isCyclic = False
iter = 0
def read():
    param = inputFile.read(1)
    inputFile.read(1);#function used to read characters and read newline after
    return param

def readGraph():
    global nrNodes
    nrNodes = int(read())
    for i in range(0,nrNodes):
        node = read()
        graph.setdefault(node, [])
        nrNeighbours = int(read())
        for j in range(nrNeighbours):
            neighbour = read()
            graph[node].append(neighbour)

def printGraph():
    for node in graph:
        print(node,end=' -> ')
        for neighbour in graph[node]:
            print(neighbour,end=' ')
        print()

def DFS(graph , node , parent = None):
    visitList.append(node)
    print(node , end = ' ')
    global nrVisited
    global isCyclic
    global full
    global nrNodes
    nrVisited += 1
    if nrVisited == nrNodes:
        full = True
    for neighbour in graph[node]:
        testVisit = 1
        for visit in visitList:
            if visit == neighbour:
                testVisit = 0
                if visit != parent:
                    isCyclic = True
        if testVisit == 1:
            DFS(graph , neighbour , node)
def findShortestPath(graph , start , end , path = []):
    path = path + [start]
    if start == end:
        return path
    shortest = None
    if start not in graph:
        return None
    for nodes in graph[start]:
        if nodes not in path:
            newPath = findShortestPath(graph,nodes,end,path)
            if newPath:
                if not shortest or len(newPath) < len(shortest):
                    shortest = newPath
    return shortest
def testIfCyclic():
    if isCyclic == True:
        print("Graph has at least one cycle")
    else:
        print("Graph has no cycles")





