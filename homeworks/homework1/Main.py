import main2
main2.readGraph()
print("The graph from input is")
main2.printGraph()
isConnected = False
iter = 0
print("Connected components are:")
for nodes in main2.graph:
    if nodes not in main2.visitList:
        main2.DFS(main2.graph,nodes)
        print()
        if(iter == 0 and main2.full == True):
            isConnected = True
        iter = iter + 1
if isConnected == True:
    print("Graph is connected")
else:
    print("Graph is not connected")
main2.testIfCyclic()
print("Insert starting node")
start = input()
print("insert ending node")
end = input()
print("shortest path from " , start , "to end is")
path = main2.findShortestPath(main2.graph,start,end)
print(path)