#include "Chicken.h"

namespace lab9_3
{
int Chicken::getEnergy() const
{
    if(m_isOrganic == true)
    {
        return (getCarbohydrates() / getFats()) * getQuantity() * 2;
    }
    else
    {
        return (getCarbohydrates() / getFats()) * getQuantity();
    }
}
int Chicken::getQuantity() const
{
    return m_quantity;
}
}