#include "Apple.h"

namespace lab9_3
{
bool Apple::isRipe() const
{
    return (getHeight() > 3 && getColor() == "red");
}
int Apple::getEnergy() const
{
    return ((getCarbohydrates() + m_vitaminC) / getFats());
}
}