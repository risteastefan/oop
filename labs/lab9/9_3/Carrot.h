#ifndef CARROT_H
#define CARROT_H

#include "Plant.h"

namespace lab9_3
{
class Carrot: public Plant
{
public:
    Carrot(const std::string &rootShape, int height, const std::string &color): m_rootShape(rootShape), Plant(height, color){}
    virtual ~Carrot() = default;
    bool isRipe() const override;
private: 
    std::string m_rootShape;
};
}

#endif //CARROT_H