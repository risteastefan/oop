#include "FoodSource.h"

namespace lab9_3
{
int FoodSource::getCarbohydrates() const
{
    return m_carbohydrates;
}
int FoodSource::getFats() const
{
    return m_fats;
}
}