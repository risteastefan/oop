#ifndef CHICKEN_H
#define CHICKEN_H

#include "FoodSource.h"
namespace lab9_3
{
class Chicken: public FoodSource
{
public:
    Chicken(bool isOrganic, int carbohydrates, int fats, int quantity): m_isOrganic(isOrganic), m_quantity(quantity), FoodSource(carbohydrates, fats){}
    virtual ~Chicken() = default;
    int getEnergy() const override;
    int getQuantity() const;
private:
    bool m_isOrganic;
    int m_quantity;
};
}

#endif //CHICKEN_H