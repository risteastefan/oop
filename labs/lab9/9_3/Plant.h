#ifndef PLANT_H
#define PLANT_H

#include <string>

namespace lab9_3
{
class Plant
{
public:
    Plant(int height, const std::string &color): m_height(height), m_color(color){};
    virtual ~Plant() = default;
    int getHeight() const;
    std::string getColor() const;
    virtual bool isRipe() const = 0;
private:
    int m_height;
    std::string m_color;
};
}

#endif //PLANT_H