#ifndef APPLE_H
#define APPLE_H

#include "FoodSource.h"
#include "Plant.h"

namespace lab9_3
{
class Apple: public Plant, public FoodSource
{
public:
    Apple(int carbohydrates, int fats, int vitaminC, int height, const std::string &color): FoodSource(carbohydrates, fats), Plant(height, color), m_vitaminC(vitaminC){}
    bool isRipe() const override;
    int getEnergy() const override;
    virtual ~Apple() = default;
private:
    int m_vitaminC;
};
}

#endif //APPLE_H