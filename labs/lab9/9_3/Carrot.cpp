#include "Carrot.h"

namespace lab9_3
{
bool Carrot::isRipe() const
{
    return(getHeight() >= 6);
}
}