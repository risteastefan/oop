#ifndef FOOD_SOURCE_H
#define FOOD_SOURCE_H

namespace lab9_3
{
class FoodSource
{
public:
    FoodSource(int carbohydrates, int fats): m_carbohydrates(carbohydrates), m_fats(fats){};
    virtual ~FoodSource() = default;
    int getCarbohydrates() const;
    int getFats() const;
    virtual int getEnergy() const = 0;
private:
    int m_carbohydrates, m_fats;
};
}

#endif //FOOD_SOURCE_H