#include "Square.h"
#include <sstream>
#include <iostream>

namespace lab9_1
{
Square::Square(const std::string &name, const std::string &color, int side) :Form(name, color), m_side(side){}
void Square::setSide(int side)
{
    m_side = side;
}
int Square::getSide() const
{
    return m_side;
}
void Square::showMessage(std::ostream &os) const
{
    os << "mesaj din Square" << "\n";
}

int Square::area() const
{
    return m_side * m_side;
}
}