#include "Form.h"

namespace lab9_1
{
void Form::setColor(const std::string &color) 
{
    m_color = color;
}
Form::Form(const std::string &name, const std::string &color): m_name(name), m_color(color){}

std::string Form::getName() const
{
    return m_name;
}
void Form::setName(const std::string &name)
{
    m_name = name;
}
void Form::showMessage(std::ostream &os) const
{
    os<<"mesaj din Form"<<"\n";
}
}