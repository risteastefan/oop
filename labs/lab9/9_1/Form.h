#ifndef FORM_H
#define FORM_H

#include <iostream>
#include <string>

namespace lab9_1
{
class Form
{
public :
    Form(const std::string &name, const std::string &color);
    virtual ~Form() = default;

protected:
    void setColor(const std::string &);
    void setName(const std::string &);
    std::string getName() const;
    virtual void showMessage(std::ostream &os) const;
    virtual int area() const = 0;

    std::string m_name;

private:
    std::string m_color;
};
};

#endif //FORM_H