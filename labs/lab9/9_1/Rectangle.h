#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Form.h"

namespace lab9_1
{
class Rectangle : public Form
{
public:
    Rectangle(const std::string &name, const std::string &color, int width, int height);
    virtual ~Rectangle() = default;
    void setWidth(int width);
    int getWidth() const;
    void showMessage(std::ostream &os) const override;
    int area() const override;
protected :
    int m_width, m_height;
};
}
#endif //RECTANGLE_H