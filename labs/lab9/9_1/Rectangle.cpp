#include <sstream>
#include "Rectangle.h"
#include <iostream>

namespace lab9_1
{
Rectangle::Rectangle(const std::string &name, const std::string &color, int width, int height): Form(name, color), m_width(width), m_height(height){}
void Rectangle::setWidth(int width)
{
    m_width = width;
}
int Rectangle::getWidth() const
{
   return m_width;
}
void Rectangle::showMessage(std::ostream &os) const
{
    os<<"mesaj din Rectangle"<<"\n";
}

int Rectangle::area() const
{
    return m_width * m_height;
}
}