#ifndef SQUARE_H
#define SQUARE_H

#include "Form.h"

namespace lab9_1
{
class Square:public Form 
{
public:
    Square(const std::string &name, const std::string &color, int side);
    virtual ~Square() = default;
    void setSide(int side);
    int getSide() const;
    void showMessage(std::ostream &os) const override;
    int area() const override;
protected:
    int m_side;
};
}

#endif //SQUARE_H