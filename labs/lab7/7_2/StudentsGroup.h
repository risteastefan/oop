#ifndef LAB7_2_H
#define LAB7_2_H

#include "Student.h"

namespace lab7_2
{
class StudentsGroup;
std::ostream& operator<<(std::ostream &, const StudentsGroup &);
struct StudentsGroup
{
public:
    StudentsGroup(const std::vector<Student> &students, const std::string &name) : m_students(students), m_name(name), m_year(1){};
    friend std::ostream& operator<<(std::ostream &, const StudentsGroup &);
    void addStudent(const Student &param);
    void removeStudent(const std::string &name);
    void changeName(const std::string &name);
    void changeStudentName(const std::string &newName, const std::string &name);
    void changeStudentAge(int age, const std::string &name);
    void changeStudentLicenseNumber(const std::string &LicenseNumber, const std::string &name);
    void changeStudentLicenseValability(int valability, const std::string &name);
    void getStudentDriverLicense(const std::string &LicenseNumber, int valability,const std::string &name);
    void removeStudentDriverLicense(const std::string &name);
    void transferStudent(const std::string name, StudentsGroup &destinationGroup);
private:
    std::vector<Student> m_students;
    int m_year;
    std::string m_name;
};
}

#endif //LAB7_2_H