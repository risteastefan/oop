#include "Student.h"

namespace lab7_2
{
std::ostream& operator<<(std::ostream &out, const Student::Person &param)
{
    out << "name: " << param.m_name << "\n" << "age: " << param.m_age << "\n";
    return out;
}

void Student::Person::setName(const std::string &name)
{
    m_name = name;
}

void Student::Person::setAge(int age)
{
    m_age = age;
}

std::ostream& operator<<(std::ostream &out, const Student::Driver &param)
{
    out << "license number: " << param.m_licenseNumber << "\n";
    out << "license valability: " << param.m_licenseValability << "\n";
    return out;
}

void Student::Driver::setLicenseNumber(const std::string &licenseNumber)
{
    m_licenseNumber = licenseNumber;
}

void Student::Driver::setLicenseValability(int licenseValability)
{
    m_licenseValability = licenseValability;
}

Student::Student(const std::string &name, int age, std::vector<int> grades): m_grades(grades), m_isDriver(false), m_driverData(NULL)
{
    m_personData = new Person(name, age);
}
Student::Student(const std::string &name, int age, const std::string &licenseNumber, int licenseValability, const std::vector<int> &grades): m_grades(grades), m_isDriver(true)
{
    m_personData = new Person(name, age);
    m_driverData = new Driver(licenseNumber, licenseValability);
}
Student::Student(const Student &param): m_grades(param.m_grades)
{
    m_personData = new Person;
    *m_personData = *param.m_personData;
    if(param.m_isDriver == true)
    {
        m_driverData = new Driver;
        *m_driverData = *param.m_driverData;
        m_isDriver = true;
    }
    else
    {
        m_driverData = NULL;
        m_isDriver = false;
    }
}
Student::~Student()
{
    delete m_personData;
    delete m_driverData;
}
std::ostream& operator<<(std::ostream &out, const Student &param)
{
    out << *param.m_personData;
    if(param.m_isDriver == true)
    {
        out << *param.m_driverData;
    }
    else
    {
        out << "Is not a driver" << std::endl;
    }
    out << "Grades : ";
    for(const auto &it : param.m_grades)
        out << it <<" ";
    out << std::endl;
    return out;
}
Student& Student::operator=(const Student &param)
{
    m_personData = new Person;
    m_personData->m_name = param.m_personData->m_name;
    m_personData->m_age = param.m_personData->m_age;
    if(param.m_isDriver == true)
    {
        m_driverData = new Driver;
        *m_driverData = *param.m_driverData;
        m_isDriver = true;
    }
    else
    {
        m_driverData = NULL;
        m_isDriver = false;
    }
    return *this;
}
}