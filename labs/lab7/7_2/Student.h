#ifndef STUDENT_H
#define STUDENT_H

#include <vector>
#include <fstream>
#include <string>

namespace lab7_2
{
class Student
{
public:
    friend class StudentsGroup;
    class Person
    {
        friend class Student;
        friend class StudentsGroup;
    private:
        std::string m_name;
        int m_age;
        Person(const std::string &name, int age): m_name(name), m_age(age) {}
        Person(): m_name(""), m_age(0){}
        friend std::ostream& operator<<(std::ostream &, const Person &);
        void setName(const std::string &name);
        void setAge(int age);
    };

    class Driver
    {
        friend class Student;
        friend class StudentsGroup;
    private:
        friend std::ostream& operator<<(std::ostream &, const Driver &);
        Driver(const std::string &licenseNumber, int licenseValability): m_licenseNumber(licenseNumber), m_licenseValability(licenseValability) {}
        Driver(): m_licenseNumber(""), m_licenseValability(0){}
        void setLicenseNumber(const std::string &licenseNumber);
        void setLicenseValability(int licenseValability);
        std::string m_licenseNumber;
        int m_licenseValability;
    };
    Student(const std::string &name, int age, std::vector<int> grades);
    Student(const std::string &name, int age, const std::string &licenseNumber, int licenseValability, const std::vector<int> &grades);
    Student(const Student &param);
    Student& operator=(const Student &param);
    ~Student();
private:
    friend std::ostream& operator<<(std::ostream &, const Student &);
    Person *m_personData;
    Driver *m_driverData;
    bool m_isDriver;
    std::vector<int> m_grades;
       
};
}

#endif //STUDENT_H