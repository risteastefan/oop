#include "StudentsGroup.h"
#include <iostream>
#include <algorithm>

namespace lab7_2
{
std::ostream& operator<<(std::ostream &out, const StudentsGroup &param)
{
    out << "Group name : " << param.m_name << std::endl;
    out << "Group year : " << param.m_year << std::endl;
    out << "Number of students in the group : " << param.m_students.size() << std::endl;
    for(const auto &it : param.m_students)
    {
        out << it;
    }
}
void StudentsGroup::addStudent(const Student &param)
{
    m_students.push_back(param);
}
void StudentsGroup::removeStudent(const std::string &name)
{
    m_students.erase(std::remove_if(m_students.begin(), m_students.end(), [name](const Student &param)
                           {
                                return param.m_personData->m_name == name;
                           }),
                            m_students.end());
}
void StudentsGroup::changeName(const std::string &name)
{
    m_name = name;
    m_year++;
}
void StudentsGroup::transferStudent(const std::string name, StudentsGroup &destinationGroup)
{   
    for(std::vector<Student>::iterator it = m_students.begin(); it != m_students.end();)
    {
        if(it->m_personData->m_name == name)
        {
            destinationGroup.addStudent(*it);
            it = m_students.erase(it);
        }
        else
        { 
            it++;
        }
    }
} 
void StudentsGroup::changeStudentName(const std::string &newName, const std::string &name)
{
    for(std::vector<Student>::iterator it = m_students.begin(); it != m_students.end(); it++)
    {
        if(it->m_personData->m_name == name)
        {
            it->m_personData->setName(newName);        
        }
    }
}
void StudentsGroup::changeStudentAge(int age, const std::string &name)
{
    for(std::vector<Student>::iterator it = m_students.begin(); it != m_students.end(); it++)
    {
        if(it->m_personData->m_name == name)
        {
            it->m_personData->setAge(age);        
        }
    }
}
void StudentsGroup::changeStudentLicenseNumber(const std::string &LicenseNumber, const std::string &name)
{
    for(std::vector<Student>::iterator it = m_students.begin(); it != m_students.end(); it++)
    {
        if(it->m_personData->m_name == name && it->m_isDriver == true)
        {
            it->m_driverData->setLicenseNumber(LicenseNumber);
        }
    }
}
void StudentsGroup::changeStudentLicenseValability(int valability, const std::string &name)
{
    for(std::vector<Student>::iterator it = m_students.begin(); it != m_students.end(); it++)
    {
        if(it->m_personData->m_name == name && it->m_isDriver == true)
        {
            it->m_driverData->setLicenseValability(valability);
        }
    }
}
void StudentsGroup::getStudentDriverLicense(const std::string &LicenseNumber, int valability, const std::string &name)
{
    for(std::vector<Student>::iterator it = m_students.begin(); it != m_students.end(); it++)
    {
        if(it->m_personData->m_name == name && it->m_isDriver == false)
        {
            it->m_driverData = new Student::Driver(LicenseNumber, valability);
            it->m_isDriver = true;
        }
    }
}
void StudentsGroup::removeStudentDriverLicense(const std::string &name)
{
    for(std::vector<Student>::iterator it = m_students.begin(); it != m_students.end(); it++)
    {
        if(it->m_personData->m_name == name && it->m_isDriver == true)
        {
            delete it->m_driverData;
            it->m_isDriver = false;
        }
    }
}
}