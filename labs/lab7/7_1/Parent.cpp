#include "Parent.h"
#include <iostream>

namespace lab7_1
{
    void Parent::printChildren(std::fstream &out)
    {
        for(const auto &child : m_children)
        {
            out << child;
            out << std::endl;
        }
    }
    
    std::fstream& operator<<(std::fstream &out, Parent &param)
    {
        out << (const Employee &) param;
        out << "Nr children : " << param.m_children.size() << std::endl << "is married : ";
        if (param.m_isMarried == true)
        {
            out << "yes" << std::endl << std::endl;
        }
        else
        {
            out << "no" << std::endl << std::endl;
        }
        out << "Childrens :" << std::endl << std::endl;
        param.printChildren(out);
        return out;
    }

    void Parent::setMarriage(bool isMarried)
    {
        m_isMarried = isMarried;
    }

    void Parent::addChild(const Child &child)
    {
        m_children.push_back(child);
    }

    std::fstream& operator<<(std::fstream &out, const Parent::Child &param)
    {
        out << (const Person &) param;
        out << "school : " << param.m_schoolName << std::endl;
        return out;
    }

    void Parent::Child::setSchool(std::string schoolName)
    {
        m_schoolName = schoolName;
    }
}