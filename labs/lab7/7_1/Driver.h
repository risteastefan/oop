#ifndef DRIVER_H
#define DRIVER_H

#include "Person.h"

namespace lab7_1
{
    class Driver;
    std::fstream& operator<<(std::fstream &, const Driver &param);
    class Driver : public Person
    {
    public:
        Driver(std::string licenseNumber, int licenseValability, std::string name, int age) : 
                            Person(name, age), m_licenseNumber(licenseNumber), m_licenseValability(licenseValability) {}
        void setLicenseNumber(std::string licenseNumber);
        void setLicenseValability(int licensevalability);
        friend std::fstream& operator<<(std::fstream &, const Driver &param);
    private:
        std::string m_licenseNumber;
        int m_licenseValability;
    };
}
#endif //DRIVER_H
