#include "Person.h"
#include <iostream>

namespace lab7_1
{
    std::fstream& operator<<(std::fstream& out, const Person &param)
    {
        out << "name : " << param.m_name << std::endl << "age : " << param.m_age << std::endl;
        return out;
    }

    void Person::setName(std::string &name)
    {
        m_name = name;
    }

    void Person::setAge(int age)
    {
        m_age = age;
    }

    std::string Person::getname() const
    {
        return m_name;
    }
}
