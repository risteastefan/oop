#include "Employee.h"
#include <iostream>

namespace lab7_1
{
    std::fstream& operator<<(std::fstream &out, const Employee &param)
    {
        out << (const Driver &)param;
        out << "company name : " << param.m_companyName << std::endl;
        return out;
    }

    void Employee::setCompanyName(std::string companyName)
    {
        m_companyName = companyName;
    }
}