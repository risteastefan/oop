#include "Employee.h"
#include <algorithm>
#include <vector>
#include <iostream>
#include "Parent.h"

namespace lab7_1
{
    void printAllDrivers(const std::vector<Driver> &drivers, std::fstream &out)
    {
        for (const auto &driver : drivers)
        {
            out << driver;
            out << std::endl;
        }
        out << std::endl;
    }

    void printAllEmployees(const std::vector<Employee> &employees, std::fstream &out)
    {
        for (const auto &employee : employees)
        {
            out << employee;
            out << std::endl;
        }
        out << std::endl;
    }

    void printAllParents(const std::vector<Parent> &parents, std::fstream &out)
    {
        for (auto parent : parents)
        {
            out << parent;
        }
        out << std::endl;
    }

    void printAllChildren(const std::vector<Parent> &parents, std::fstream &out)
    {
        for (auto parent : parents)
        {
            for (auto child : parent.m_children)
            {
                out << child;
                out << std::endl;
            }
        }
    }

    void addDriver(const Driver &driver, std::vector<Driver> &drivers)
    {
        drivers.push_back(driver);
    }

    void addEmployee(const Employee &employee, std::vector<Employee> &employees)
    {
        employees.push_back(employee);
    }

    void addParent(const Parent &parent, std::vector<Parent> &parents)
    {
        parents.push_back(parent);
    }


    void deleteDriver(std::string name, std::vector<Driver> &drivers)
    {
        for (auto it = drivers.begin() ; it != drivers.end(); ++it )
        {
            if (it->getname() == name)
            {
                drivers.erase(it);
                break;
            }
        }
    }

    void deleteEmployee(std::string name, std::vector<Employee> &employees)
    {
        for (auto it = employees.begin(); it != employees.end(); ++it)
        {
            if (it->getname() == name)
            {
                employees.erase(it);
                break;
            }
        }
    }
}