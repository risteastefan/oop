#include <fstream>

namespace lab7_1
{
    void printAllDrivers(const std::vector<Driver> &driversstd, std::fstream &);
    void printAllEmployees(const std::vector<Employee> &employees, std::fstream &);
    void printAllParents(const std::vector<Parent> &parents, std::fstream &);

    void addDriver(const Driver &driver, std::vector<Driver> &drivers);
    void addEmployee(const Employee &employee, std::vector<Employee> &employees);
    void addParent(const Parent &parent, std::vector<Parent> &parents);

    void deleteDriver(std::string name, std::vector<Driver> &drivers);
    void deleteEmployee(std::string name, std::vector<Employee> &employees);
}
