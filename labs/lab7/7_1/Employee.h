#ifndef  EMPLOYEE_H
#define EMPLOYEE_H

#include "Driver.h"
#include <string>

namespace lab7_1
{
    class Employee;
    std::fstream& operator<<(std::fstream&, const Employee &param);
    class Employee : public Driver
    {
    public:
        Employee(std::string companyName, std::string licenseNumber, int licenseValability, std::string name, int age) :
                        Driver(licenseNumber, licenseValability, name, age), m_companyName(companyName) {}
        void setCompanyName(std::string companyName);
        friend std::fstream& operator<<(std::fstream&, const Employee &param);
    private:
        std::string m_companyName;
    };
}

#endif //EMPLOYEE_H