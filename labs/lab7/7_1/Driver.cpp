#include "Driver.h"
#include <iostream>

namespace lab7_1
{
    std::fstream& operator<<(std::fstream &out, const Driver &param)
    {
        out << (const Person &) param;
        out << "license number : " << param.m_licenseNumber << std::endl;
        out << "license expiration date : " << param.m_licenseValability << std::endl;
        return out;
    }

    void Driver::setLicenseNumber(std::string licenseNumber)
    {
        m_licenseNumber = licenseNumber;
    }

    void Driver::setLicenseValability(int licenseValability)
    {
        m_licenseValability = licenseValability;
    }
}