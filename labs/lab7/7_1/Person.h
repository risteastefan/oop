#ifndef PERSON_H
#define PERSON_H

#include <string>
#include <fstream>
#include <sstream>

namespace lab7_1
{
    class Person;
    std::fstream& operator<<(std::fstream& out, const Person &param);
    class Person
    {
    public:
        Person(std::string &name, int age) : m_name(name), m_age(age) {}
        void setName(std::string &name);
        void setAge(int age);
        std::string getname()const;
        friend std::fstream& operator<<(std::fstream& out, const Person&param);
    private:
        std::string m_name;
        int m_age;
    };
}

#endif //PERSON_H
