#ifndef PARENT_H
#define PARENT_H

#include "Employee.h"
#include <vector>
#include <fstream>

namespace lab7_1
{
    class Parent;
    class Child;
    void printAllChildren(const std::vector<Parent> &parents, std::fstream &out);
    std::fstream& operator<<(std::fstream &, const Child &param);
    std::fstream& operator<<(std::fstream &, Parent &param);

    class Parent :public Employee
    {
    public:
        friend void printAllChildren(const std::vector<Parent> &parents, std::fstream &out);
        friend std::fstream& operator<<(std::fstream &, Parent &param);
        class Child : public Person
        {
        public:
            Child(std::string schoolName, std::string name, int age) : Person(name, age), m_schoolName(schoolName) {}
            void setSchool(std::string school);
            friend std::fstream& operator<<(std::fstream &, const Child &param);
        private:
            std::string m_schoolName;
        };
    public:
        Parent(bool isMarried, std::string companyName, std::string licenseNumber, 
                        int licenseValability, std::string name, int age) : Employee(companyName, licenseNumber, licenseValability, name, age), m_isMarried(isMarried){}
        void setMarriage(bool isMarried);
        void addChild(const Child &child);
        void printChildren(std::fstream &out);
    private:
        std::vector<Child> m_children;
        bool m_isMarried;
    };
}

#endif //PARENT_H
