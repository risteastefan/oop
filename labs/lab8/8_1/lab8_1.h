namespace lab8_1
{
template <class T>
T GetMax(T a, T b) 
{ 
    T result;   
    result = (a>b) ? a : b;
    return (result); 
}

template <class T>
T GetMin(T a, T b)
{
    T result;
    result = (a < b) ? a : b;
    return (result);
}
}