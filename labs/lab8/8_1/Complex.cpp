#include "Complex.h"
#include <cmath>

namespace lab8_1
{
bool Complex::operator<(const Complex &param) const
{ 
    return (distance() < param.distance() ? true : false); 
}
double Complex::getReal() const
{ 
    return m_real; 
}
double Complex::getImag() const
{
    return m_imag; 
}
bool equalDoubles(double d1, double d2)
{
    double eps = 0.0001;
    return (std::abs(d1 - d2) < eps);
}
bool Complex::operator==(const Complex &param) const
{
    return (equalDoubles(m_real, param.m_real) && equalDoubles(m_imag, param.m_imag));
}
double Complex::distance() const 
{
    return m_real * m_real + m_imag * m_imag; 
}
}