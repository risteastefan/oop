namespace lab8_1
{
bool equalDoubles(double, double);
class Complex
{
    public:
    Complex(double real, double imag): m_real(real), m_imag(imag) {};
    Complex(): m_real(0), m_imag(0) {};
    double getReal() const;
    double getImag() const;
    bool operator<(const Complex &param) const;
    bool operator==(const Complex &param) const;
private:
    double m_real;
    double m_imag;
    double distance() const;
};
}