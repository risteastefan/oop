#include "Complex.h"
#include <iostream>
namespace lab6_2
{
class Operations
{
public:
	static double module(double param);
	static double module(const Complex &param);
	static void print(double, std::ostream&);	
	static void print(const Complex &param, std::ostream&);	
};
}