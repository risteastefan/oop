namespace lab6_2
{
class Complex
{
	double m_real, m_imag;
public:
	Complex(double real, double imag): m_real(real), m_imag(imag) {};
	double getReal() const;
	double getImag() const;
};
}