#ifndef COMPLEX_H
#define COMPLEX_H
#include <fstream>

namespace lab6_3
{
    bool equalDoubles(double, double);
    class Complex;
    std::ostream& operator<<(std::ostream&, const Complex&);
    class Complex
    {
        double m_real, m_imag;
    public:
        Complex();
        Complex(double real, double imag) : m_real(real), m_imag(imag) {};
        double getReal() const;
        double getImag() const;
        void setReal(double);
        void setImag(double);
        bool operator==(const Complex &param) const;
        friend std::ostream& operator<<(std::ostream&, const Complex&);
    };
};

#endif //COMPLEX_H