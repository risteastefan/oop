#include "Operations.h"
#include <climits>
#include <iostream>

namespace lab6_3
{
    double Operations::sum(double param1, double param2) const 
    {
        return param1 + param2;
    }

    double Operations::difference(double param1, double param2) const 
    {
        return param1 - param2;
    }

    double Operations::multiplication(double param1, double param2) const
    {
        return param1 * param2;
    }  

    double Operations::division(double param1, double param2) const
    {
        return param1 / param2;
    }

    Complex Operations::sum(const Complex &param1, const Complex &param2)
    {
        Complex nr(param1.getReal() + param2.getReal(), param1.getImag() + param2.getImag());
        return nr;
    }

    Complex Operations::difference(const Complex &param1, const Complex &param2)
    {
        Complex nr(param1.getReal() - param2.getReal(), param1.getImag() - param2.getImag());
        return nr;
    }

    Complex Operations::multiplication(const Complex &param1, const Complex &param2)
    {
        Complex nr(param1.getReal() * param2.getReal() - param1.getImag() * param2.getImag(), param1.getReal() * param2.getImag() + param1.getImag() * param2.getReal());
        return nr;
    }

    Complex Operations::division(const Complex &param1, const Complex &param2)
    {
        if (param2.getReal() == 0 && param2.getImag() == 0)
        {
            return Complex(INT_MAX, INT_MAX);
        }
        double realNominator = param1.getReal() * param2.getReal() + param1.getImag() * param2.getImag();
        double imagNominator = param1.getImag() * param2.getReal() - param1.getReal() * param2.getImag();
        double denominator = param2.getReal() * param2.getReal() + param2.getImag() * param2.getImag();
        Complex nr(realNominator / denominator, imagNominator / denominator);
        return nr;
    }
}
