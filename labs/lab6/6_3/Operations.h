#include "Complex.h"
#include <string>

namespace lab6_3
{
    class Operations
    {
    public:
        double sum(double, double) const;
        Complex sum(const Complex&, const Complex&);
        double difference(double, double) const;
        Complex difference(const Complex&, const Complex&);
        double multiplication(double, double) const;
        Complex multiplication(const Complex&, const Complex&);
        double division(double, double) const;
        Complex division(const Complex&, const Complex&);
    };
};