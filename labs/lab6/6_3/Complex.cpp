#include "Complex.h"
#include <cmath>

namespace lab6_3
{
    bool equalDoubles(double d1, double d2)
    {
        double eps = 0.0001;
        return (std::abs(d1 - d2) < eps);
    }

    double Complex::getReal() const
    {
        return m_real;
    }

    double Complex::getImag() const
    {
        return m_imag;
    }

    void Complex::setReal(double param)
    {
        m_real = param;
    }

    void Complex::setImag(double param)
    {
        m_imag = param;
    }

    Complex::Complex()
    {
        m_real = 0;
        m_imag = 0;
    }

    bool Complex::operator==(const Complex &param) const
    {
        return (equalDoubles(m_real, param.m_real) && equalDoubles(m_imag, param.m_imag));
    }

    std::ostream& operator<<(std::ostream& os, const Complex &param)
    {
       os << param.getReal() << " + " << param.getImag() << " * i";
    }
}