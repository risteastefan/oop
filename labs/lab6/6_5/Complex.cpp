#include "Complex.h"
#include <cmath>
#include <climits>

namespace lab6_5
{
bool equalDoubles(double d1, double d2)
{
    double eps = 0.0001;
    return (std::abs(d1 - d2) < eps);
}

double Complex::operator~() const
{
    return (sqrt(m_imag * m_imag + m_real * m_real));
}

Complex& Complex::operator*=(const Complex& param)
{
    double tmpReal = m_real;
    double tmpImag = m_imag;
    m_real = (tmpReal * param.m_real) - (tmpImag * param.m_imag);
    m_imag = (tmpReal * param.m_imag) + (tmpImag * param.m_real);
    return *this;
}

Complex Complex::operator^(int power) const
{
    Complex nr = *this;
    for (int i = 0; i < power - 1; i++)
        nr *= *this;
    return nr;
}

bool Complex::operator==(const Complex &param) const
{
    return (equalDoubles(m_real, param.m_real) && equalDoubles(m_imag, param.m_imag));
}

Complex Complex::operator+(const Complex &param) const
{
    Complex nr;
    nr.m_real = m_real + param.m_real;
    nr.m_imag = m_imag + param.m_imag;
    return nr;
}

Complex Complex::operator-(const Complex &param) const
{
    Complex nr;
    nr.m_real = m_real - param.m_real;
    nr.m_imag = m_imag - param.m_imag;
    return nr;
}

Complex Complex::operator*(const Complex &param) const
{
    Complex nr = *this;
    nr *= param;
    return nr;
}

Complex Complex::operator/(const Complex &param) const
{
    Complex nr;
    if((m_real == 0 && m_imag == 0) || (param.m_real == 0 && param.m_imag == 0))
    {
        nr.m_real = INT_MAX;
        nr.m_imag = INT_MAX;
        return nr;
    }
    nr.m_real = (m_real * param.m_real + m_imag * param.m_imag) / (param.m_real * param.m_real + param.m_imag * param.m_imag);
    nr.m_imag = (param.m_real * m_imag - m_real * param.m_imag) / (param.m_real * param.m_real + param.m_imag * param.m_imag);
    return nr;
}
}