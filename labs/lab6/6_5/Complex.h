#ifndef COMPLEX_H
#define COMPLEX_H

namespace lab6_5
{
bool equalDoubles(double d1, double d2);

class Complex
{
    double m_real;
    double m_imag;
public:
    Complex() {};
    Complex(double real, double imag) : m_real(real), m_imag(imag) {};
    double operator~() const;
    Complex operator^(int power) const;
    Complex& operator*=(const Complex&);
    Complex operator+(const Complex&) const;
    Complex operator-(const Complex&) const;
    Complex operator*(const Complex&) const;
    Complex operator/(const Complex&) const;
    bool operator==(const Complex &) const;
};
}

#endif //COMPLEX_H
