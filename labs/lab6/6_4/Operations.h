#include "Complex.h"
#include <string>

namespace lab6_4
{
    class Operations
    {
    public:
        static double sum(double, double);
        static Complex sum(const Complex&, const Complex&);
        static double difference(double, double);
        static Complex difference(const Complex&, const Complex&);
        static double multiplication(double, double);
        static Complex multiplication(const Complex&, const Complex&);
        static double division(double, double);
        static Complex division(const Complex&, const Complex&);
    };
};