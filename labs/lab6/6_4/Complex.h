#include <fstream>

namespace lab6_4
{
    bool equalDoubles(double, double);
    class Complex;
    std::ostream& operator<<(std::ostream&, const Complex&);
    class Complex
    {
        double m_real, m_imag;
    public:
        Complex() : m_real(0), m_imag(0){};
        Complex(double real, double imag) : m_real(real), m_imag(imag) {};
        double getReal() const;
        double getImag() const;
        void setReal(double);
        void setImag(double);
        bool operator==(const Complex& param) const;
        friend std::ostream& operator<<(std::ostream&, const Complex&);
    };
};