#include "Operations.h"
#include <sstream>
#include <cmath>

namespace lab6_1
{
double Operations::module(double param) const 
{
	return param < 0 ? -param : param; 
}

double Operations::module(const Complex &param) const 
{ 
	return sqrt(param.getReal() * param.getReal() + param.getImag() * param.getImag()); 
}  

void Operations::print(double param, std::ostream& os = std::cout) 
{
	os << param;
}	

void Operations::print(const Complex &param, std::ostream& os = std::cout) 
{
	os << param.getReal() << " + " << param.getImag() << " * i";
}	
}
