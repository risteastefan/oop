#include "Complex.h"
#include <iostream>

namespace lab6_1
{
class Operations
{
public:
	double module(double param) const;
	double module(const Complex &param) const;
	void print(double, std::ostream&);	
	void print(const Complex &param, std::ostream&);	
};
}
