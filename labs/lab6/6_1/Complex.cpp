#include "Complex.h"

namespace lab6_1
{
double Complex::getReal() const
{
	return m_real;
}

double Complex::getImag() const
{
	return m_imag;
}
}