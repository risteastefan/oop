#include "stdafx.h"

#include "studentgroup.h"

StudentsGroup::StudentsGroup(int studentsNumber)
{
        this->m_root = NULL;
        this->m_studentsNumber = studentsNumber;
        readStudentsInGroup();
}

void StudentsGroup::readStudentsInGroup()
{
        StudentNode * iterator;    
        for(int i = 0 ; i < this->m_studentsNumber ; i++)
        {
                string studentName;
                int studentNumber;
                cout<<"insert student "<<i+1<<" name\n";
                cin>>studentName;
                cout<<"insert student "<<i+1<<" note\n";
                cin>>studentNumber;
                if(m_root == NULL)
                {
                         m_root = new StudentNode;
                         m_root->m_student = Student(studentNumber , studentName);
                         m_root->m_next = NULL;
                         m_tail = m_root;
                }
                else
                {
                        StudentNode * newNode = new StudentNode();
                        newNode->m_student = Student(studentNumber , studentName);
                        newNode->m_next = NULL;
                        m_tail->m_next = newNode;
                        m_tail = m_tail->m_next;
                }
        }
}

void StudentsGroup::showStudentsInGroup()
{
        if(m_root == NULL)
        {
                cout<<"Error : list is empty , could show students\n";
        }
        StudentNode * iterator;    
        iterator = m_root;
        for(int i = 0 ; i < m_studentsNumber ; i++)
        {
                cout<<i<<" "<<iterator->m_student.getNote()<<" "<<iterator->m_student.getName()<<"\n";
                iterator = iterator->m_next;
        }
}

StudentsGroup::~StudentsGroup()
{
       StudentNode * iterator;    
       iterator = m_root;
       StudentNode *next;
       while(iterator != NULL)
       {
                next = iterator->m_next;
                delete iterator;
                iterator = next;
       }
}

void StudentsGroup::sortStudentsInGroup()
{
        if(m_root == NULL)
        {
                cout<<"Error : list is empty , could not sort\n";
                return;
        }
        StudentNode * iterator;    
        iterator = m_root;
        StudentNode * iterator2;
        while(iterator->m_next != NULL)
        {
                iterator2 = iterator->m_next;
                while(iterator2 != NULL)
                {
                        if(iterator->m_student.getName() > iterator2->m_student.getName())
                        {
                                Student tmp = iterator->m_student;
                                iterator->m_student = iterator2->m_student;
                                iterator2->m_student = tmp;
                        }
                        iterator2 = iterator2->m_next;
                }
                iterator = iterator->m_next;
        }
}

Student StudentsGroup::findHighestNote()
{
        if(m_root == NULL)
        {
                cout<<"Error : list is empty , could not find student\n";
        }
        StudentNode * iterator;    
        Student student;
        int maxNote = 0;
        iterator = m_root;
        while(iterator != NULL)
        {
                if(iterator->m_student.getNote() > maxNote)
                {
                        maxNote = iterator->m_student.getNote();
                        student = iterator->m_student;
                }
                iterator = iterator->m_next;
        }
        return student;
}

Student* StudentsGroup::findFirst5Note(int &ok)
{
        if(m_root == NULL)
        {
                cout<<"Error : list is empty , could not find student\n";
                ok = 0;
                return NULL;
        }
        StudentNode * iterator;    
        iterator = m_root;
        while(iterator != NULL)
        {
                if(iterator->m_student.getNote() == 5)
                        return &iterator->m_student;
                iterator = iterator->m_next;
        }
        return NULL;
}