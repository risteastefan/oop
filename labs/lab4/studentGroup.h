#include "student.h"

struct StudentNode
{
        Student m_student;
        StudentNode * m_next;
};

class StudentsGroup
{
        int m_studentsNumber;
        StudentNode * m_root;
        StudentNode * m_tail;
public:
        StudentsGroup(int studentsNumber);      
        void showStudentsInGroup();
        void sortStudentsInGroup();
        void readStudentsInGroup();
        Student findHighestNote();
        Student* findFirst5Note(int &ok);
        ~StudentsGroup();
};