#include "stdafx.h"

#include "student.h"

Student::Student(int studentNote, string studentName)
{
        this->m_note = studentNote;
        this->m_name = studentName;
}

string Student::getName()
{
        return this->m_name;
}

int Student::getNote()
{
        return this->m_note;
}

void Student::setName(string s)
{
        this->m_name = s;
}

void Student::setNote(int n)
{
        this->m_note= n;
}
Student& Student::operator= (const Student& param)
{
        m_name = param.m_name;
        m_note = param.m_note;
        return *this;
}