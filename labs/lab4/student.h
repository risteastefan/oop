#include "string"
#include "iostream"
using namespace std;

class Student
{
private:
        int m_note;
        string m_name;
public:
        Student(int studentNote = 0, string studentName = "Joe Doe");
        Student& operator= (const Student& param);
        void setNote(int n);
        int getNote();
        void setName(string s);
        string getName();
};