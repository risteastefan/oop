// Lab4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "studentGroup.h"

int main()
{
        int nrStudents;
        cout<<"insert number of students\n";
        cin>>nrStudents;
        StudentsGroup *group = new StudentsGroup(nrStudents);
        group->sortStudentsInGroup();
        group->showStudentsInGroup();
        int ok = 1;
        Student *student = group->findFirst5Note(ok);
        if(student != NULL)
                cout<<"First student with grade 5 is :"<<student->getName()<<"\n";
        else if(student == NULL && ok == 1)
                cout<<"No 5 grade";
        delete group;
	return 0;
}

