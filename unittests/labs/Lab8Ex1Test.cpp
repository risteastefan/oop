#include "gtest/gtest.h"
#include "labs/lab8/8_1/Complex.h"
#include "labs/lab8/8_1/lab8_1.h"

using namespace lab8_1;

class Lab8_1 : public ::testing::Test {

protected:
    Lab8_1() {}
    virtual ~Lab8_1() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab8_1, testMinComplexWhenDecimalsAre0) 
{
    Complex nr1(2,3);
    Complex nr2(3,4);
    ASSERT_EQ(nr1, GetMin(nr1,nr2));
}

TEST_F(Lab8_1, testMaxComplexWithInts) 
{
    int nr1 = 3;
    int nr2 = 4;
    ASSERT_EQ(nr2, GetMax(nr1,nr2));
}

TEST_F(Lab8_1, testMinComplexWhenDecimalsAreNot0) 
{
    Complex nr1(2.2314,3.3242);
    Complex nr2(3.231,4.23432);
    ASSERT_EQ(nr1, GetMin(nr1,nr2));
}

TEST_F(Lab8_1, testMaxDoubleWhenDecimalsAreNot0) 
{
    double nr1 = 4.3242;
    double nr2 = 3.231;
    ASSERT_TRUE(equalDoubles(nr1, GetMax(nr1,nr2)));
}

TEST_F(Lab8_1, testMinComplexWhenNumbersAreEqual) 
{
    Complex nr1(2,3);
    Complex nr2(2,3);
    ASSERT_EQ(nr1, GetMin(nr1,nr2));
}

TEST_F(Lab8_1, testMaxDoubleWhenNumbersAreEqual) 
{
    double nr1 = 3.6786;
    double nr2 = 3.6786;
    ASSERT_TRUE(equalDoubles(nr1, GetMax(nr1,nr2)));
}