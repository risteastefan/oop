#include "gtest/gtest.h"
#include "labs/lab6/6_2/Operations.h"
#include <iostream>
#include <climits>

using namespace lab6_2;

class Lab6_2 : public ::testing::Test {

protected:
    Lab6_2() {}
    virtual ~Lab6_2() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab6_2, testModuleDoubleWhenAllDecimalsAre0) 
{
    double nr = 4.0;
    ASSERT_NEAR(4.000, Operations::module(nr), 0.001); 
}

TEST_F(Lab6_2, testModuleComplexWhenAllDecimalsAre0) 
{
    Complex nr(3, 4);
    ASSERT_NEAR(5.000, Operations::module(nr), 0.0001); 
}

TEST_F(Lab6_2, testModuleDoubleWhenNumberIsNegativeAndDecimalsAreNot0) 
{
    double nr = - 4.7;
    ASSERT_NEAR(4.7, Operations::module(nr), 0.0001); 
}

TEST_F(Lab6_2, testModuleComplexWhenDecimalsAreNot0) 
{
    Complex nr(3.345, 4.13);
    ASSERT_NEAR(5.314, Operations::module(nr), 0.01); 
}

TEST_F(Lab6_2, testModuleComplexWhenNumberIs0) 
{
    Complex nr(0, 0);
    ASSERT_NEAR(0, Operations::module(nr), 0.01); 
}

TEST_F(Lab6_2, testModuleDoubleWhenNumberIs0) 
{
    double nr = 0;
    ASSERT_NEAR(0, Operations::module(nr), 0.01); 
}

TEST_F(Lab6_2, testPrintDoubleWhenAllDecimalsAre0) 
{
    double nr = 4;
    std::ostringstream oss;
    Operations::print(nr, oss);
    ASSERT_EQ(oss.str(), "4");
}

TEST_F(Lab6_2, testPrintDoubleWhenDecimalsAreNot0) 
{
    double nr = 5.3412;
    std::ostringstream oss;
    Operations::print(nr, oss);
    ASSERT_EQ(oss.str(), "5.3412");
}

TEST_F(Lab6_2, testPrintComplexWhenAllDecimalsAre0) 
{
    Complex nr(5,3);
    std::ostringstream oss;
    Operations::print(nr, oss);
    ASSERT_EQ(oss.str(), "5 + 3 * i");
}

TEST_F(Lab6_2, testPrintComplexWhenDecimalsAreNot0) 
{
    Complex nr(5.3452, 3.1234);
    std::ostringstream oss;
    Operations::print(nr, oss);
    ASSERT_EQ(oss.str(), "5.3452 + 3.1234 * i");
}
