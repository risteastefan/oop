#include "gtest/gtest.h"
#include "labs/lab7/7_2/StudentsGroup.h"
#include <iostream>
#include <vector>

using namespace lab7_2;

bool testOutput(std::string filename1, std::string filename2)
{
    std::fstream output(filename1);
    std::fstream expectedOuput(filename2);
    while (!output.eof() && !expectedOuput.eof())
    {
        std::string str1, str2;
        getline(output, str1);
        getline(expectedOuput, str2);
        if (str1 != str2)
        {
            return false;
        }
    }
    if(!output.eof() || !expectedOuput.eof())
        return false;
    return true;
}

class Lab7_2 : public ::testing::Test {

protected:
    Lab7_2() {}
    virtual ~Lab7_2() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab7_2, testPrintStudents) 
{
    std::ofstream output("7_2Output1.out", std::fstream::out);
    std::fstream expectedOutput("7_2ExpectedOutput1.out");
    std::vector<Student>students;
    students.reserve(100);
    students.push_back(Student("Stefi", 20, "12345", 2, {7, 7, 8}));
    students.push_back(Student("Dani", 19, "22322", 3, {3, 4, 5}));
    students.push_back(Student("Alex", 20, {5, 3, 10}));
    students.push_back(Student("Denis", 20, {7, 7, 8}));
    students.push_back(Student("Modan", 21, "43243", 4, {8, 8, 8}));
    students.push_back(Student("Malina", 20, {6, 7, 8}));
    StudentsGroup grp(students, "CE1");
    output << grp;      
    ASSERT_TRUE(testOutput("7_2Output1.out", "7_2ExpectedOutput1.out"));
}

TEST_F(Lab7_2, testChangePersonData)
{
    std::fstream output("7_2Output2.out", std::fstream::out);
    std::fstream expectedOutput("7_2ExpectedOutput2.out");
    std::vector<Student>students;
    students.push_back(Student("Stefi", 20, "12345", 2, {7, 7, 8}));
    StudentsGroup grp(students, "CE1");
    grp.changeStudentName("Andrei", "Stefi");
    grp.changeStudentAge(29, "Andrei");
    grp.changeStudentName("Alex", "Raz");
    grp.changeStudentAge(29, "Raz");
    output << grp;
    ASSERT_TRUE(testOutput("7_2Output2.out", "7_2ExpectedOutput2.out"));
}

TEST_F(Lab7_2, testChangeDriverData)
{
    std::fstream output("7_2Output3.out", std::fstream::out);
    std::fstream expectedOutput("7_2ExpectedOutput3.out");
    std::vector<Student> students;
    students.push_back(Student("Stefi", 20, "12345", 2, {7, 7 , 8}));
    students.push_back(Student("Andrei", 29, {8, 9, 10}));
    StudentsGroup grp(students, "CE1");
    grp.changeStudentLicenseNumber("3245", "Stefi");
    grp.changeStudentLicenseValability(1, "Stefi");
    grp.getStudentDriverLicense("33223", 5, "Andrei");
    grp.getStudentDriverLicense("324324", 5, "Stefi");
    grp.getStudentDriverLicense("324324", 5, "Raz");
    output << grp;
    ASSERT_TRUE(testOutput("7_2Output3.out", "7_2ExpectedOutput3.out"));
}

TEST_F(Lab7_2, testChangeGroupData)
{
    std::fstream output("7_2Output4.out", std::fstream::out);
    std::fstream expectedOutput("7_2ExpectedOutput4.out");
    std::vector<Student>students1;
    students1.push_back(Student("Stefi", 20, "12345", 2, {7, 7 , 8}));
    students1.push_back(Student("Andrei", 29, {8, 9, 10}));
    StudentsGroup grp1(students1, "CE1");
    grp1.changeName("CE2");
    grp1.removeStudent("Tyt");
    std::vector<Student>students2;
    students2.push_back(Student("Raz", 30, {8, 9, 10}));
    StudentsGroup grp2(students2, "CR1");
    grp1.transferStudent("Andrei", grp2);
    grp2.removeStudent("Raz");
    output << grp1;
    output << grp2;
    ASSERT_TRUE(testOutput("7_2Output4.out", "7_2ExpectedOutput4.out"));
}