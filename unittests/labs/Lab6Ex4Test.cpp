#include "gtest/gtest.h"
#include "labs/lab6/6_4/Operations.h"
#include <iostream>
#include <climits>

using namespace lab6_4;

class Lab6_4 : public ::testing::Test {

protected:
    Lab6_4() {}
    virtual ~Lab6_4() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab6_4, testDoubleSumWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    double nr3 = Operations::sum(nr1, nr2);
    ASSERT_NEAR(6, nr3, 0.0001);
}

TEST_F(Lab6_4, testDoubleSumWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    double nr3 = Operations::sum(nr1, nr2);
    ASSERT_NEAR(6.577, nr3, 0.001);
}

TEST_F(Lab6_4, testComplexSumWhenAllDecimalsAre0) 
{
    Complex nr1(2,3), nr2(4,7);
    Complex nr3 = Operations::sum(nr1, nr2);
    ASSERT_EQ(nr3, Complex(6, 10));
}

TEST_F(Lab6_4, testComplexSumWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Complex nr3 = Operations::sum(nr1, nr2);
    ASSERT_EQ(nr3, Complex(5.355, 7.988));
}

TEST_F(Lab6_4, testDoubleDifferenceWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    double nr3 = Operations::difference(nr1, nr2);
    ASSERT_NEAR(-2, nr3, 0.0001);
}

TEST_F(Lab6_4, testDoubleDifferenceWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    double nr3 = Operations::difference(nr1, nr2);
    ASSERT_NEAR(-2.113, nr3, 0.001);
}

TEST_F(Lab6_4, testComplexDifferenceWhenAllDecimalsAre0) 
{
    Complex nr1(5,3), nr2(4,6);
    Complex nr3 = Operations::difference(nr1, nr2);
    ASSERT_EQ(nr3, Complex(1, -3));
}

TEST_F(Lab6_4, testComplexDifferenceWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Complex nr3 = Operations::difference(nr1, nr2);
    ASSERT_EQ(nr3, Complex(1.109, 1.146));
}

TEST_F(Lab6_4, testDoubleMultiplicationWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    double nr3 = Operations::multiplication(nr1, nr2);
    ASSERT_NEAR(8, nr3, 0.0001);
}

TEST_F(Lab6_4, testDoubleMultiplicationWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    double nr3 = Operations::multiplication(nr1, nr2);
    ASSERT_NEAR(9.698, nr3, 0.001);
}

TEST_F(Lab6_4, testComplexMultiplicationWhenAllDecimalsAre0) 
{
    Complex nr1(5,3), nr2(4,6);
    Complex nr3 = Operations::multiplication(nr1, nr2);
    ASSERT_EQ(nr3, Complex(2, 42));
}

TEST_F(Lab6_4, testComplexMultiplicationWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Complex nr3 = Operations::multiplication(nr1, nr2);
    ASSERT_EQ(nr3, Complex(-8.762171, 20.752413));
}

TEST_F(Lab6_4, testDoubleDivisionWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    double nr3 = Operations::division(nr1, nr2);    
    ASSERT_NEAR(0.5, nr3, 0.0001);
}

TEST_F(Lab6_4, testDoubledivisionWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    double nr3 = Operations::division(nr1, nr2);
    ASSERT_NEAR(0.513, nr3, 0.001);
}

TEST_F(Lab6_4, testComplexDivisonWhenAllDecimalsAre0) 
{
    Complex nr1(5,3), nr2(4,6);
    Complex nr3 = Operations::division(nr1, nr2);
    ASSERT_EQ(nr3, Complex(0.73076, -0.346153));
}

TEST_F(Lab6_4, testComplexDivisionWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Complex nr3 = Operations::division(nr1, nr2);
    ASSERT_EQ(nr3, Complex(1.38709, -0.08395));
}

TEST_F(Lab6_4, testComplexPrint) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    std::ostringstream oss;
    oss << Operations::division(nr1, nr2);;
    ASSERT_EQ(oss.str(), "1.38709 + -0.0839543 * i");
}