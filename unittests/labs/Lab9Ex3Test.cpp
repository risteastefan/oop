#include "gtest/gtest.h"
#include "labs/lab9/9_3/Chicken.h"
#include "labs/lab9/9_3/Carrot.h"
#include "labs/lab9/9_3/Apple.h"
#include <iostream>
#include <climits>

using namespace lab9_3;

class Lab9_3 : public ::testing::Test {

protected:
    Lab9_3() {}
    virtual ~Lab9_3() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab9_3, testVirtualFunctionsApplePointer) 
{
    Apple *c = new Apple(3, 3, 2, 4, "red");
    ASSERT_EQ(1, c->getEnergy());
    ASSERT_TRUE(c->isRipe());
}

TEST_F(Lab9_3, testVirtualFunctionsFoodSourcePointer) 
{
    FoodSource *c = new Apple(3, 3, 2, 4, "red");
    ASSERT_EQ(1,c->getEnergy());
    //c->isRipe() This will give an error, because FoodSoutce does not have isRipe() function
}

TEST_F(Lab9_3, testVirtualFunctionsPlantPointer) 
{
    Plant *c = new Apple(3, 3, 2, 4, "red"); 
    //c->getEnergy() This will give an error, because Plant does not have getEnergy() function
    ASSERT_TRUE(c->isRipe());
}

TEST_F(Lab9_3, testVirtualFunctionsChickenPointer) 
{
    Chicken *c = new Chicken(true, 5, 2, 3); 
    ASSERT_EQ(12, c->getEnergy());
}

TEST_F(Lab9_3, testVirtualFunctionsCarrotPointer) 
{
    Carrot *c = new Carrot("denver", 5, "orange"); 
    ASSERT_FALSE(c->isRipe());
}