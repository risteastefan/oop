#include "gtest/gtest.h"
#include "labs/lab6/6_5/Complex.h"
#include <iostream>
#include <climits>

using namespace lab6_5;

class Lab6_5 : public ::testing::Test {

protected:
    Lab6_5() {}
    virtual ~Lab6_5() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab6_5, testModuleWhenAllDecimalsAre0) 
{
    Complex nr(3, 4);
    ASSERT_NEAR(5.00000, ~nr, 0.0001);
}

TEST_F(Lab6_5, testPowerWhenAllDecimalsAre0) 
{
    Complex nr(3, 4);
    nr = nr ^ 2;
    ASSERT_EQ(nr, Complex(-7.00000,24.0000));
}

TEST_F(Lab6_5, testModuleWhenDecimalsAreDifferentThan0) 
{
    Complex nr(2, 6);
    ASSERT_NEAR(6.3245,~nr, 0.0001);
}

TEST_F(Lab6_5, testPowerWhenDecimalsAreDifferentThan0) 
{
    Complex nr(3.4, 3.232);
    nr = nr^4;
    ASSERT_EQ(nr, Complex(-481.77351,48.97382));
}

TEST_F(Lab6_5, testModuleWhenNumberIsNull) 
{
    Complex nr(0, 0);
    ASSERT_NEAR(0, ~nr, 0.0001);
}

TEST_F(Lab6_5, testPowerWhenNumberIsNull)
{
    Complex nr(0, 0);
    nr = nr ^ 3;
    ASSERT_EQ(nr, Complex(0, 0));
}

TEST_F(Lab6_5, testSumOperator)
{
    Complex nr1(2, 3);
    Complex nr2(4, 36);
    Complex nr;
    nr = nr1 + nr2;
    ASSERT_EQ(nr, Complex(6,39));
}

TEST_F(Lab6_5, testDifferenceOperator)
{
    Complex nr1(2, 3);
    Complex nr2(4, 36);
    Complex nr;
    nr = nr1 - nr2;
    ASSERT_EQ(nr, Complex(-2,-33));
}

TEST_F(Lab6_5, testDivisionOperator)
{
    Complex nr1(2, 3);
    Complex nr2(4, 36);
    Complex nr;
    nr = nr1 / nr2;
    ASSERT_EQ(nr, Complex(0.08841,-0.04573));
}

TEST_F(Lab6_5, testDivisionOperatorWhenDenominatorIs0)
{
    Complex nr1(2, 3);
    Complex nr2(0, 0);
    Complex nr;
    nr = nr1 / nr2;
    ASSERT_EQ(nr, Complex(INT_MAX, INT_MAX));
}

TEST_F(Lab6_5, testMultiplicationOperator)
{
    Complex nr1(3, 3);
    Complex nr2(4, 5);
    Complex nr;
    nr = nr1 * nr2;
    ASSERT_EQ(nr, Complex(-3, 27));
}

TEST_F(Lab6_5, testMultiplicationOperatorWhenATermIs0)
{
    Complex nr1(2, 3);
    Complex nr2(0, 0);
    Complex nr;
    nr = nr1 * nr2;
    ASSERT_EQ(nr, Complex(0, 0));
}