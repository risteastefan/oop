#include "gtest/gtest.h"
#include "labs/lab7/7_1/Parent.h"
#include "labs/lab7/7_1/Employee.h"
#include "labs/lab7/7_1/lab7_1.h"
#include <climits>
#include <fstream>

using namespace lab7_1;

namespace lab7_1
{
    bool testOutput(std::string filename1, std::string filename2)
    {
        std::fstream output(filename1);
        std::fstream expectedOuput(filename2);
        while (!output.eof() && !expectedOuput.eof())
        {
            std::string str1, str2;
            getline(output, str1);
            getline(expectedOuput, str2);
            if (str1 != str2)
            {
                return false;
            }
        }
        if(!output.eof() || !expectedOuput.eof())
        {
            return false;
        }
        return true;
    }
}
class Lab7_1 : public ::testing::Test {
protected:
    Lab7_1() {}
    virtual ~Lab7_1() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab7_1, testDriversBasic) 
{
    std::fstream output("7_1Output1.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput1.out");

    std::vector<Driver> drivers;
    addDriver(Driver("1234", 2, "Nicu", 45), drivers);
    addDriver(Driver("1234", 4, "Viorel", 40), drivers);

    printAllDrivers(drivers, output);

    ASSERT_TRUE(testOutput("7_1Output1.out", "7_1ExpectedOutput1.out"));
}

TEST_F(Lab7_1, testDriversDelete) 
{
    std::fstream output("7_1Output2.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput2.out");

    std::vector<Driver> drivers;
    addDriver(Driver("1234", 2, "Nicu", 45), drivers);
    addDriver(Driver("1234", 4, "Viorel", 40), drivers);

    deleteDriver("Nicu", drivers);

    printAllDrivers(drivers, output);

    ASSERT_TRUE(testOutput("7_1Output2.out", "7_1ExpectedOutput2.out"));
}

TEST_F(Lab7_1, testDriversDeleteAllDrivers) 
{
    std::fstream output("7_1Output3.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput3.out");

    std::vector<Driver> drivers;
    addDriver(Driver("1234", 2, "Nicu", 45), drivers);
    addDriver(Driver("1234", 4, "Viorel", 40), drivers);

    deleteDriver("Nicu", drivers);
    deleteDriver("Viorel", drivers);

    printAllDrivers(drivers, output);

    ASSERT_TRUE(testOutput("7_1Output3.out", "7_1ExpectedOutput3.out"));
}

TEST_F(Lab7_1, testEmployeesBasic) 
{
    std::fstream output("7_1Output4.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput4.out");

    std::vector<Employee> employees;
    addEmployee(Employee("Metalcom", "1234", 2, "Victor", 45), employees);
    addEmployee(Employee("BRD", "1234", 4, "Silviua", 40), employees);

    printAllEmployees(employees, output);

    ASSERT_TRUE(testOutput("7_1Output4.out", "7_1ExpectedOutput4.out"));
}

TEST_F(Lab7_1, testEmployeesDelete) 
{
    std::fstream output("7_1Output5.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput5.out");

    std::vector<Employee> employees;
    addEmployee(Employee("Metalcom", "1234", 2, "Victor", 45), employees);
    addEmployee(Employee("BRD", "1234", 4, "Silvia", 40), employees);

    deleteEmployee("Victor", employees);

    printAllEmployees(employees, output);

    ASSERT_TRUE(testOutput("7_1Output5.out", "7_1ExpectedOutput5.out"));
}

TEST_F(Lab7_1, testEmployeesDeleteAll) 
{
    std::fstream output("7_1Output6.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput6.out");

    std::vector<Employee> employees;
    addEmployee(Employee("Metalcom", "1234", 2, "Victor", 45), employees);
    addEmployee(Employee("BRD", "1234", 4, "Silvia", 40), employees);

    deleteEmployee("Victor", employees);
    deleteEmployee("Silvia", employees);

    printAllEmployees(employees, output);

    ASSERT_TRUE(testOutput("7_1Output6.out", "7_1ExpectedOutput6.out"));
}

TEST_F(Lab7_1, testParentsBasic) 
{
    std::fstream output;
    output.open("7_1Output7.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput7.out");

    std::vector<Parent> parents;
    addParent(Parent(true, "Metalcom", "1234", 2, "Victor", 45), parents);
    addParent(Parent(true, "BRD", "1234", 4, "Silviua", 40), parents);

    parents[0].addChild(Parent::Child("Carol", "Alex", 10));
    parents[0].addChild(Parent::Child("Cuza", "Denis", 15));
    parents[1].addChild(Parent::Child("Buzesti", "Vlad", 13));

    printAllParents(parents, output);

    ASSERT_TRUE(testOutput("7_1Output7.out", "7_1ExpectedOutput7.out"));
    output.close();
};

TEST_F(Lab7_1, testParentsEmpty) 
{
    std::fstream output;
    output.open("7_1Output8.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput8.out");

    std::vector<Parent> parents;

    printAllParents(parents, output);

    ASSERT_TRUE(testOutput("7_1Output8.out", "7_1ExpectedOutput8.out"));
}

TEST_F(Lab7_1, testPrintAllChildren) 
{
    std::fstream output;
    output.open("7_1Output9.out", std::fstream::out);
    std::fstream expectedOutput("7_1ExpectedOutput9.out");

    std::vector<Parent> parents;
    addParent(Parent(true, "Metalcom", "1234", 2, "Victor", 45), parents);
    addParent(Parent(true, "BRD", "1234", 4, "Silviua", 40), parents);

    parents[0].addChild(Parent::Child("Carol", "Alex", 10));
    parents[0].addChild(Parent::Child("Cuza", "Denis", 15));
    parents[1].addChild(Parent::Child("Buzesti", "Vlad", 13));

    printAllChildren(parents, output);

    ASSERT_TRUE(testOutput("7_1Output9.out", "7_1ExpectedOutput9.out"));
}