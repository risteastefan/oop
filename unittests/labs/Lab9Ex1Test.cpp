#include "gtest/gtest.h"
#include "labs/lab9/9_1/Rectangle.h"
#include "labs/lab9/9_1/Square.h"

using namespace lab9_1;

class Lab9_1 : public ::testing::Test 
{

protected:
    Lab9_1() {}
    virtual ~Lab9_1() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab9_1, testRectangleMessage) 
{
    Rectangle rect("rect1", "red", 3, 4);
    std::ostringstream oss;
    rect.showMessage(oss);
    ASSERT_EQ("mesaj din Rectangle\n", oss.str());
}

TEST_F(Lab9_1, testRectangleArea) 
{
    Rectangle rect("rect1", "red", 3, 4);
    ASSERT_EQ(12, rect.area());
}

TEST_F(Lab9_1, testSquareMessage) 
{
    Square square("square1", "blue", 5);
    std::ostringstream oss;
    square.showMessage(oss);
    ASSERT_EQ("mesaj din Square\n", oss.str());
}

TEST_F(Lab9_1, testSquareArea) 
{
    Square square("square2", "purple", 7);
    ASSERT_EQ(49, square.area());
}