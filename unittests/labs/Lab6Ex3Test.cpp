#include "gtest/gtest.h"
#include "labs/lab6/6_3/Operations.h"
#include <iostream>
#include <climits>

using namespace lab6_3;

class Lab6_3 : public ::testing::Test {

protected:
    Lab6_3() {}
    virtual ~Lab6_3() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab6_3, testDoubleSumWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    Operations op;
    double nr3 = op.sum(nr1, nr2);
    ASSERT_NEAR(6, nr3, 0.0001);
}

TEST_F(Lab6_3, testDoubleSumWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    Operations op;
    double nr3 = op.sum(nr1, nr2);
    ASSERT_NEAR(6.577, nr3, 0.001);
}

TEST_F(Lab6_3, testComplexSumWhenAllDecimalsAre0) 
{
    Complex nr1(2,3), nr2(4,7);
    Operations op;
    Complex nr3 = op.sum(nr1, nr2);
    ASSERT_EQ(nr3, Complex(6, 10));
}

TEST_F(Lab6_3, testComplexSumWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Operations op;
    Complex nr3 = op.sum(nr1, nr2);
    ASSERT_EQ(nr3, Complex(5.355, 7.988));
}

TEST_F(Lab6_3, testDoubleDifferenceWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    Operations op;
    double nr3 = op.difference(nr1, nr2);
    ASSERT_NEAR(-2, nr3, 0.0001);
}

TEST_F(Lab6_3, testDoubleDifferenceWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    Operations op;
    double nr3 = op.difference(nr1, nr2);
    ASSERT_NEAR(-2.113, nr3, 0.001);
}

TEST_F(Lab6_3, testComplexDifferenceWhenAllDecimalsAre0) 
{
    Complex nr1(5,3), nr2(4,6);
    Operations op;
    Complex nr3 = op.difference(nr1, nr2);
    ASSERT_EQ(nr3, Complex(1, -3));
}

TEST_F(Lab6_3, testComplexDifferenceWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Operations op;
    Complex nr3 = op.difference(nr1, nr2);
    ASSERT_EQ(nr3, Complex(1.109, 1.146));
}

TEST_F(Lab6_3, testDoubleMultiplicationWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    Operations op;
    double nr3 = op.multiplication(nr1, nr2);
    ASSERT_NEAR(8, nr3, 0.0001);
}

TEST_F(Lab6_3, testDoubleMultiplicationWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    Operations op;
    double nr3 = op.multiplication(nr1, nr2);
    ASSERT_NEAR(9.698, nr3, 0.001);
}

TEST_F(Lab6_3, testComplexMultiplicationWhenAllDecimalsAre0) 
{
    Complex nr1(5,3), nr2(4,6);
    Operations op;
    Complex nr3 = op.multiplication(nr1, nr2);
    ASSERT_EQ(nr3, Complex(2, 42));
}

TEST_F(Lab6_3, testComplexMultiplicationWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Operations op;
    Complex nr3 = op.multiplication(nr1, nr2);
    ASSERT_EQ(nr3, Complex(-8.762171, 20.752413));
}

TEST_F(Lab6_3, testDoubleDivisionWhenAllDecimalsAre0) 
{
    double nr1 = 2, nr2 = 4;
    Operations op;
    double nr3 = op.division(nr1, nr2);
    ASSERT_NEAR(0.5, nr3, 0.0001);
}

TEST_F(Lab6_3, testDoubledivisionWhenDecimalsAreNot0) 
{
    double nr1 = 2.232, nr2 = 4.345;
    Operations op;
    double nr3 = op.division(nr1, nr2);
    ASSERT_NEAR(0.513, nr3, 0.001);
}

TEST_F(Lab6_3, testComplexDivisonWhenAllDecimalsAre0) 
{
    Complex nr1(5,3), nr2(4,6);
    Operations op;
    Complex nr3 = op.division(nr1, nr2);
    ASSERT_EQ(nr3, Complex(0.73076, -0.346153));
}

TEST_F(Lab6_3, testComplexDivisionWhenDecimalsAreNot0) 
{
    Complex nr1(3.232,4.567), nr2(2.123, 3.421);
    Operations op;
    Complex nr3 = op.division(nr1, nr2);
    ASSERT_EQ(nr3, Complex(1.38709, -0.08395));
}

TEST_F(Lab6_3, testComplexPrint) 
{
    Complex nr(3.232,4.567);
    std::ostringstream oss;
    oss << nr;
    ASSERT_EQ("3.232 + 4.567 * i", oss.str());
}
