#include "gtest/gtest.h"
#include "labs/lab6/6_1/Operations.h"
#include <iostream>
#include <climits>

using namespace lab6_1;

class Lab6_1 : public ::testing::Test {

protected:
    Lab6_1() {}
    virtual ~Lab6_1() = default;

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(Lab6_1, testModuleDoubleWhenAllDecimalsAre0) 
{
    double nr = 4.0;
    Operations op;
    ASSERT_NEAR(4.000, op.module(nr), 0.001); 
}

TEST_F(Lab6_1, testModuleComplexWhenAllDecimalsAre0) 
{
    Complex nr(3, 4);
    Operations op;
    ASSERT_NEAR(5.000, op.module(nr), 0.0001); 
}

TEST_F(Lab6_1, testModuleDoubleWhenNumberIsNegativeAndDecimalsAreNot0) 
{
    double nr = - 4.7;
    Operations op;
    ASSERT_NEAR(4.7, op.module(nr), 0.0001); 
}

TEST_F(Lab6_1, testModuleComplexWhenDecimalsAreNot0) 
{
    Complex nr(3.345, 4.13);
    Operations op;
    ASSERT_NEAR(5.314, op.module(nr), 0.01); 
}

TEST_F(Lab6_1, testModuleComplexWhenNumberIs0) 
{
    Complex nr(0, 0);
    Operations op;
    ASSERT_NEAR(0, op.module(nr), 0.01); 
}

TEST_F(Lab6_1, testModuleDoubleWhenNumberIs0) 
{
    double nr = 0;
    Operations op;
    ASSERT_NEAR(0, op.module(nr), 0.01); 
}

TEST_F(Lab6_1, testPrintDoubleWhenAllDecimalsAre0) 
{
    double nr = 4;
    Operations op;
    std::ostringstream oss;
    op.print(nr, oss);
    ASSERT_EQ(oss.str(), "4");
}

TEST_F(Lab6_1, testPrintDoubleWhenDecimalsAreNot0) 
{
    double nr = 5.3412;
    Operations op;
    std::ostringstream oss;
    op.print(nr, oss);
    ASSERT_EQ(oss.str(), "5.3412");
}

TEST_F(Lab6_1, testPrintComplexWhenAllDecimalsAre0) 
{
    Complex nr(5,3);
    Operations op;
    std::ostringstream oss;
    op.print(nr, oss);
    ASSERT_EQ(oss.str(), "5 + 3 * i");
}

TEST_F(Lab6_1, testPrintComplexWhenDecimalsAreNot0) 
{
    Complex nr(5.3452, 3.1234);
    Operations op;
    std::ostringstream oss;
    op.print(nr, oss);
    ASSERT_EQ(oss.str(), "5.3452 + 3.1234 * i");
}
